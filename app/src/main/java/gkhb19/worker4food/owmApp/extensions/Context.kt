package gkhb19.worker4food.owmApp.extensions

import android.content.Context
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.settings.*

//Configuration

val Context.isLandscape: Boolean
    get() = resources.getBoolean(R.bool.isLand)

val Context.isPortrait: Boolean
    get() = !this.isLandscape

// Preferences

fun Context.readSettings(): AppSettings = AppSettings.fromPrefs(
        getSharedPreferences(prefsFileName, Context.MODE_PRIVATE),
        resources
)

fun Context.saveSettings(conf: AppSettings) = conf.toPrefs(
    getSharedPreferences(prefsFileName, Context.MODE_PRIVATE)
)

