package gkhb19.worker4food.owmApp.extensions

import androidx.fragment.app.Fragment
import gkhb19.worker4food.owmApp.settings.AppSettings

val Fragment.isLandscape: Boolean
    get() = requireContext().isLandscape

val Fragment.isPortrait: Boolean
    get() = requireContext().isPortrait

fun Fragment.readSettings(): AppSettings = requireContext().readSettings()

fun Fragment.saveSettings(conf: AppSettings) = requireContext().saveSettings(conf)
