package gkhb19.worker4food.owmApp.fragments


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import gkhb19.worker4food.owmApp.R
import gkhb19.worker4food.owmApp.viewmodels.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_error.*

class ErrorFragment : Fragment() {

    private val model by activityViewModels<WeatherViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_error, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = findNavController()

        btnAppSettings.setOnClickListener {
            controller.navigate(R.id.settingsFragment)
        }

        btnSysSettings.setOnClickListener {
            Intent(Settings.ACTION_WIRELESS_SETTINGS)
                .takeIf { it.resolveActivity(requireContext().packageManager) != null }
                ?.also(::startActivity)
                ?: Toast.makeText(
                        requireContext(),
                        R.string.no_wireless_settings,
                        Toast.LENGTH_SHORT
                    ).show()
        }

        btnTryAgain.setOnClickListener {
            model.updateWeatherInfo()
        }

        model.error.observe(viewLifecycleOwner, Observer {
            if (it != null) errorText.text = it
            else controller.navigateUp()
        })
    }
}
