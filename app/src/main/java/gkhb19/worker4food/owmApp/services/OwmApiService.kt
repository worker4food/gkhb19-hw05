package gkhb19.worker4food.owmApp.services

import android.app.Application
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.*
import okhttp3.*
import retrofit2.*
import java.util.concurrent.TimeUnit
import okhttp3.MediaType.Companion.toMediaType

const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
const val API_KEY = "2230dd43c6cbde51975e349d75f261e9"

val MEDIA_TYPE = "application/json".toMediaType()

class OwmApiService(private val app: Application) {

    val client: OwmApi
        get() = retrofit.create(OwmApi::class.java)

    private val retrofit: Retrofit
        get() {
            val converter = Json.nonstrict.asConverterFactory(MEDIA_TYPE)

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(converter)
                .client(okHttpClient)
                .build()
        }

    private val okHttpClient: OkHttpClient
        get() = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(Interceptors.auth(API_KEY))
            .addInterceptor(ChuckerInterceptor(app))
            .addInterceptor(Interceptors.prettier(MEDIA_TYPE))
            .build()
}
